import sys
sys.path.append('./')
from flask import Blueprint
from flask_restful import Api
from retrieval_code import RetrievalCodeResource

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(RetrievalCodeResource, '/retrieval_code')
