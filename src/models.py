import sys
sys.path.append('./')
from flask import Flask
from flask_mongoalchemy import MongoAlchemy
from run import app, db

class Segment(db.Document):
    code = db.StringField()
    name = db.StringField()
    score = db.FloatField(required=False)

class Family(db.Document):
    segment = db.DocumentField(Segment)
    code = db.StringField()
    name = db.StringField()
    score = db.FloatField(required=False)

class _Class(db.Document):
    family = db.DocumentField(Family)
    code = db.StringField()
    name = db.StringField()
    score = db.FloatField(required=False)

class MaterialType(db.Document):
    _class = db.DocumentField(_Class)
    code = db.StringField()
    name = db.StringField()
    score = db.FloatField(required=False)

class Product(db.Document):
    material_type = db.DocumentField(MaterialType)
    code = db.StringField()
    name = db.StringField()
    description = db.StringField()
    score = db.FloatField(required=False)
