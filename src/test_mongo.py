import sys
sys.path.append('./')
from models import Segment, Family, _Class, MaterialType, Product
from run import db
from mongoalchemy.query import Query, QueryResult

if __name__ == '__main__':

    segment = Segment(name='Material Vivo Vegetal y Animal, Accesorios y Suministros', code='10')
    family = Family(name='Animales vivos', code='10', segment=segment)
    _class = _Class(name='Animales de granja', code='15', family=family)
    material_type = MaterialType(name='Gatos', code='01', _class=_class)
    product = Product(name='Gato 1', description='un gato de color amarillo', code='10101501', material_type=material_type)

    segment.save()
    family.save()
    _class.save()
    material_type.save()
    product.save()

    ## prod = Product.query.filter(Product.code == '10101501').all()
    #prod = Product.query.raw_output().filter({"$text": {"$search": "gato"}}).all()
    cursor = db.session.db.Product.find({"$text": {"$search": "gato"}}, { "score": { "$meta": "textScore" } }).sort([('score', {'$meta': 'textScore'})])
    result = QueryResult(db.session, cursor, Product, raw_output=False)
    for p in result:
        print(f"({p.score}), {p.code}|{p.name}: '{p.description}'")