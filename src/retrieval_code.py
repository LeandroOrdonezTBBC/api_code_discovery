import sys
sys.path.append('./')
from flask import request
from flask_restful import Resource

from sqlalchemy import create_engine
from run import db
from models import Segment, Family, _Class, MaterialType, Product
from mongoalchemy.query import Query, QueryResult


class RetrievalCodeResource(Resource):

	def post(self):
		print("[RetrievalCodeResource] Post method")
		json_data = request.get_json(force=True)
		query = json_data['query']
		print(f"[RetrievalCodeResource] query={query}")
		data = []
		try:
			#prod = Product.query.filter(Product.name == query).all()
			cursor = db.session.db.Product.find({"$text": {"$search": query}}, { "score": { "$meta": "textScore" } }).sort([('score', {'$meta': 'textScore'})])
			prod = QueryResult(db.session, cursor, Product, raw_output=False)
			print(f"[RetrievalCodeResource] prod={prod}")
			for p in prod:
				pr = {
					"score": p.score,
					"code": p.code,
					"name": p.name,
					"description": p.description
				}

				if float(pr["score"]) > 0.3:
					data.append(pr)


			return {'status': 'success', 'data': data}, 200
		except Exception as e:
			return {'status': 'error', 'data': e}
