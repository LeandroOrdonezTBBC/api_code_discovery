use codex;

db.Product.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.MaterialType.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.getCollection("_Class").createIndex( //https://stackoverflow.com/q/24309685/2412831
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.Family.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.Segment.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);
