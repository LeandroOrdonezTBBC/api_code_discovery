FROM python:3.7-slim

ENV FLASK_APP="run.py"
ENV APP_SETTINGS="development"
ENV API_URL_PREFIX="/bizbuy/api/1.0"
ENV MDB_DATABASE="codex"
ENV MDB_SERVER="mongodb"
ENV MDB_PORT="27017"

WORKDIR /app

ADD ./src /app
ADD ./clasificador_de_bienes_y_servicios.csv /app

RUN apt-get update && apt-get install -y htop python-dev python3-dev nano
COPY requirements.txt /tmp/ 
RUN pip install -r /tmp/requirements.txt

CMD ["python", "run.py"]